#!/usr/bin/env python3

"""
This script filters the providers and extracts those matching specific criteria.
"""

from collections import defaultdict
from datetime import datetime, timedelta

from common import *

RESULT_FILE_PATH = "results/%s.json"

criteriaA = {
	"inBandRegistration": lambda v : v,
	"ratingXmppComplianceTester": lambda v : v >= 90,
	"ratingImObservatoryClientToServer": lambda v : "A" in v,
	"ratingImObservatoryServerToServer": lambda v : "A" in v,
	"maximumHttpFileUploadFileSize": lambda v : v == 0 or v >= 20,
	"maximumHttpFileUploadTotalSize": lambda v : v == 0 or v >= 100,
	"maximumHttpFileUploadStorageTime": lambda v : v == 0 or v >= 7,
	"maximumMessageArchiveManagementStorageTime": lambda v : v == 0 or v >= 7,
	"professionalHosting": lambda v : v,
	"freeOfCharge": lambda v : v,
	"legalNotice": lambda v : len(v) != 0,
	"serverLocations": lambda v : len(v) != 0,
	"groupChatSupport,chatSupport,emailSupport": lambda v1, v2, v3 : len(v1) != 0 or len(v2) != 0 or len(v3) != 0,
	"onlineSince": lambda v : datetime.fromisoformat(v) < datetime.now() - timedelta(365),
}

criteriaB = {
	"inBandRegistration,registrationWebPage": lambda v1, v2 : v1 or len(v2) != 0,
	"ratingXmppComplianceTester": lambda v : v >= 90,
	"ratingImObservatoryClientToServer": lambda v : "A" in v,
	"ratingImObservatoryServerToServer": lambda v : "A" in v,
	"maximumHttpFileUploadFileSize": lambda v : v >= 0,
	"maximumHttpFileUploadTotalSize": lambda v : v >= 0,
	"maximumHttpFileUploadStorageTime": lambda v : v >= 0,
	"maximumMessageArchiveManagementStorageTime": lambda v : v >= 0,
}

criteriaC = {}

class ApplicationArgumentParser(ArgumentParser):
	"""This is a parser for the arguments provided to run the application."""

	def __init__(self):
		super().__init__()

		self.description = "Filters the providers and extracts those matching specific criteria."
		self.usage = "%(prog)s [-h] | [-q | -d] [-s] [-r] [-A | -B | -C] [providers]"

		self.add_argument(
			"-q",
			"--quiet",
			help="log only errors",
			action="store_const",
			dest="log_level",
			const=logging.ERROR,
			default=logging.INFO,
		)

		self.add_argument(
			"-d",
			"--debug",
			help="log debug output",
			action="store_const",
			dest="log_level",
			const=logging.DEBUG,
			default=logging.INFO,
		)

		self.add_argument(
			"-s",
			"--simple",
			help="output only provider domains instead of their properties",
			action="store_true",
			dest="simple",
		)

		self.add_argument(
			"-r",
			"--result-files",
			help="create additional files each containing the result of one provider",
			action="store_true",
			dest="result_files",
		)

		self.add_argument(
			"-A",
			"--category-A",
			help="output only providers of category A",
			action="store_const",
			dest="category",
			const=Category.AUTOMATICALLY_CHOSEN,
			default=Category.ALL,
		)

		self.add_argument(
			"-B",
			"--category-B",
			help="output only providers of category B",
			action="store_const",
			dest="category",
			const=Category.MANUALLY_SELECTABLE,
			default=Category.ALL,
		)

		self.add_argument(
			"-C",
			"--category-C",
			help="output only providers of category C",
			action="store_const",
			dest="category",
			const=Category.USABLE_FOR_AUTOCOMPLETE,
			default=Category.ALL,
		)

		self.add_argument(
			"providers",
			help="domains of providers being filtered",
			nargs='*',
		)

def create_provider_list(provider_data, providers, category, domains_only, results):
	"""Creates a file for providers of a specific category.

	Parameters
	----------
	provider_data : dict
		provider data being filtered
	providers : list
		domains of providers being filtered or an empty list for filtering all providers
	category : Category
		category used for filtering
	domains_only: bool
		whether to output a list of provider domains instead of the properties
	results: dict
		results of the filtering
	"""

	logging.debug("STARTING creation of provider list for category %s" % category.value)

	providers_count = len(providers)
	extracted_providers = []

	for jid, properties in provider_data.items():
		if providers_count == 0 or jid in providers:
			logging.debug("  %s: Filtering" % jid)

			properties = create_simple_output(jid, properties.copy())

			if filter_provider(category, properties, results[jid][category.value]):
				extracted_providers.append(properties)

	if providers_count == 0:
		providers_count = len(provider_data)

	logging.debug("RESULT: %s of %s providers in category %s" % (len(extracted_providers), providers_count, category.value))
	logging.debug("The criteria are specified in the README: https://invent.kde.org/melvo/xmpp-providers#criteria")

	if domains_only:
		extracted_providers = [provider["jid"] for provider in extracted_providers]

	# A newline is appended because Python's JSON module does not add one.
	formatted_json_string = json.dumps(extracted_providers, indent=JSON_OUTPUT_INDENTATION) + "\n"

	providers_file_path_parts = os.path.splitext(PROVIDERS_FILE_PATH)
	providers_file_name = providers_file_path_parts[0]
	providers_file_extension = providers_file_path_parts[1]

	provider_list_file_path = "%s-%s%s" % (providers_file_name, category.value, providers_file_extension)
	with open(provider_list_file_path, "w") as provider_list_file:
		provider_list_file.write(formatted_json_string)
		logging.info("'%s' created" % provider_list_file_path)

def create_simple_output(jid, properties):
	"""Creates a simple output for the properties of a provider consisting only of the information relevant to the client.

	Parameters
	----------
	jid : str
		JID of the provider
	properties : dict
		properties of the provider

	Returns
	-------
	dict
		only consisting of relevant properties
	"""

	del properties["lastCheck"]
	new_properties = {"jid": jid}

	for property_name, property_content in properties.items():
		if "content" in property_content:
			new_properties[property_name] = property_content["content"]
		else:
			new_properties[property_name] = dict()
			for language_code, language_specific_content in property_content.items():
				content = language_specific_content["content"]
				if len(content) != 0 :
					new_properties[property_name][language_code] = language_specific_content["content"]

	return new_properties

def filter_provider(category, properties, results):
	"""Filters properties by a passed category.

	Parameters
	----------
	category : Category
		category used for filtering
	properties : dict
		properties of the provider
	results: dict
		results of the filtering

	Returns
	-------
	bool
		whether the provider belongs to the category
	"""

	if category == Category.USABLE_FOR_AUTOCOMPLETE:
		return check_properties(category, properties, criteriaC, results)

	if category == Category.MANUALLY_SELECTABLE:
		return check_properties(category, properties, criteriaB, results)

	if category == Category.AUTOMATICALLY_CHOSEN:
		return check_properties(category, properties, criteriaA, results)

	return False

def check_properties(category, properties, criteria, results):
	"""Checks if properties meet specific criteria.

	Parameters
	----------
	category : Category
		category used for filtering
	properties : dict
		properties of the provider
	criteria : dict
		criteria that are checked
	results: dict
		results of the check

	Returns
	-------
	bool
		whether all properties meet the criteria
	"""

	check_succeeded = True

	for property_string, criterion in criteria.items():
		property_names = property_string.split(",")
		property_values = []

		for property_name in property_names:
			property_values.append(properties[property_name])

		if not criterion(*property_values):
			property_names_string = ", ".join(property_names)
			property_values_string = str(property_values)[1:-1]

			results[property_names_string] = property_values_string
			logging.debug("    %s: %s not meeting the criterion for category %s" % (property_names_string, property_values_string, category.value))

			if check_succeeded:
				check_succeeded = False

	return check_succeeded

def create_result_files(results):
	"""Creates files for the results of the filtering.

	Parameters
	----------
	results: dict
		results of the filtering
	"""

	create_parent_directories(RESULT_FILE_PATH)

	for jid, result in results.items():
		# A newline is appended because Python's JSON module does not add one.
		formatted_json_string = json.dumps(result, indent=JSON_OUTPUT_INDENTATION) + "\n"

		result_file_path = RESULT_FILE_PATH % jid
		with open(result_file_path, "w") as result_file:
			result_file.write(formatted_json_string)
			logging.info("'%s' created" % result_file_path)

if __name__ == "__main__":
	arguments = ApplicationArgumentParser().parse_args()
	logging.basicConfig(level=arguments.log_level, format="%(levelname)s %(message)s")

	with open(PROVIDERS_FILE_PATH, "r") as providers_file:
		try:
			provider_data = json.load(providers_file)

			category = arguments.category
			categories = [category]

			if category == Category.ALL:
				categories = [
					Category.AUTOMATICALLY_CHOSEN,
					Category.MANUALLY_SELECTABLE,
					Category.USABLE_FOR_AUTOCOMPLETE,
				]

			results = defaultdict(lambda: defaultdict(lambda: defaultdict(str)))

			for category in categories:
				create_provider_list(provider_data, arguments.providers, category, arguments.simple, results)

			if arguments.result_files:
				create_result_files(results)

		except json.decoder.JSONDecodeError as e:
			logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s" % (PROVIDERS_FILE_PATH, e.msg, e.lineno, e.colno))
