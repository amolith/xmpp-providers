# XMPP Providers - Curated List of Providers for Registration and Autocomplete

[![XMPP Providers: A](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/badge-count-A.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-A.json?job=filtered-provider-lists)
[![XMPP Providers: B](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/badge-count-B.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-B.json?job=filtered-provider-lists)
[![XMPP Providers: C](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/badge-count-C.svg?job=badges)](https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/providers-C.json?job=filtered-provider-lists)

This project provides a machine-readable curated list of [XMPP](https://xmpp.org/about/technology-overview/) providers and a script for filtering it.
The JSON list can be used by XMPP clients for provider suggestions.

Each **client** has different requirements on a provider: The script in this repository can be used to create a **statically filtered** provider list.

Each **user** has different requirements on a provider: At runtime, the statically filtered list can be **dynamically filtered** on the user's demands.

## Main Goals

This project has two main goals:
1. Simplifying the onboarding of new XMPP users by a list of XMPP providers that can be integrated into XMPP clients and used for choosing a provider for registration
1. Improving the providers' features, security, support and documentation by defining high quality standards and providing information to achieve them

## Badges

If you are a provider, you can embed a badge showing your category in your website (replace `example.org` with your domain):
```
<a href="https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/results/example.org.json?job=filtered-provider-lists"><img src="https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/raw/badges/example.org.svg?job=badges"></a>
```

The properties that do not meet the criteria for being in a specific category are provided when you click on the badge.

## Contributing

Help us to make *free communication* possible and contribute to this project!

**Please read the [contribution guidelines](/CONTRIBUTING.md) carefully before creating a merge request.**
That saves you and the reviewers a lot of time in the review process.

## Categories

The script to create a filtered list can be used for specific requirements.
However, to provide a standardized way of categorizing providers, there are three main options for filtering the list.

The following categories are used to distinguish between different quality levels the checked provders can offer.
Category A offers the best, B an average and C the worst user experience during onboarding and further usage.

The category A is a subset of B which is in turn a subset of C.
Thus, providers of A can be used for the purposes of B and C as well.
Providers of B can be used for the purposes of C as well.
The unfiltered provider list corresponds to category C.

### Category A: Automatically Chosen

Providers in this category can be used for an automatic registration.

### Category B: Manually Selectable

Providers in this category can be used for a manual registration.

### Category C: Usable for Completely Customized Filtering and Autocomplete

Providers in this category can be used for completely customized filtering and autocomplete.

## Usage Example

A filtered provider list is primarily used for registering an account with the XMPP client [Kaidan](https://invent.kde.org/network/kaidan).
The whole provider list is also going to be used to suggest and autocomplete the chat address when entering a chat address in Kaidan (e.g., while adding a contact).

Here are steps used by Kaidan, other clients might process the list similarly:
1. This project's provider list is filtered to create a list of providers which support registration (category A or B) (a) and another one which includes all providers (category C) (b).
1. The created lists are included into Kaidan's builds.
1. All providers in (a) that are good enough for belonging to category A are filtered at runtime and used to create an account automatically with [Kaidan's quick onboarding](https://www.kaidan.im/2020/01/08/Easy-Registration/).
1. All providers in (a) that are not good enough for belonging to category A but B are filtered at runtime and used to be displayed for Kaidan's manual registration.
1. All providers of (b) are used at runtime for chat address autocomplete.

Note:
Providers can support registration via [XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html) (preferred way) or via web registration (on a web page).
Therefore, if the provider does not support in-band registration, it must at least support web registration to be included in Kaidan's registration provider list.

## Properties

The properties of a provider are mapped to its server address (JID, e.g., example.org).

## Basic Information

The following properties do **not affect** the provider's **category**:

Information (Key in JSON File) | Description / Data Type / Unit of Measurement
---|---
lastCheck | [ISO YYYY-MM-DD date](https://en.wikipedia.org/wiki/ISO_8601#Dates) (e.g., 2021-01-16)
website | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of website language version or {} for n/a
busFactor | minimum number of people that have to leave before the provider becomes inoperable or -1 for n/a
company | true or false
passwordReset | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of web page language version used for automatic password reset (e.g., via email) / web page describing how to manually reset password (e.g., by contacting the provider) or {} for n/a

## Criteria

The table shows the following two circumstances for category A and B:

A provider is in a specific category if it meets all of the criteria listed in the table below.
A condition can be `true`, `false` or true if a specific case such as greater or lower than a specific value is applicable.
If the data type is a list or mapping, a condition including numbers corresponds to the count of their elements.
If the data type is a date, a condition including numbers corresponds to the age in days.

Here is an example for the in-band and web registration:
*A provider that has no inBandRegistration is not in category A.*
*But if the provider has a registrationWebPage, it is in category B if it also meets all of the other criteria for B.*

The following properties **affect** the provider's **category**:

Criterion (Key in JSON File) | Description / Data Type / Unit of Measurement | Category A | Category B
---|---|---|---
[inBandRegistration](https://xmpp.org/extensions/xep-0077.html#usecases-register) | true if registration via XMPP client supported, otherwise false | true | true or registrationWebPage >= 1
registrationWebPage | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of web page language version or {} for n/a | >= 1 or inBandRegistration | >= 1 or inBandRegistration
rating[XmppComplianceTester](https://compliance.conversations.im) | score (number in percentage) | >= 90 | >= 90
rating[ImObservatory](https://xmpp.net)ClientToServer | score (upper-case letter) | >= A | >= A
rating[ImObservatory](https://xmpp.net)ServerToServer | score (upper-case letter) | >= A | >= A
maximum[HttpFileUpload](https://xmpp.org/extensions/xep-0363.html)FileSize | -1 for n/a, 0 for no limit or number in megabytes (MB) | 0 or >= 20 | >= 0
maximum[HttpFileUpload](https://xmpp.org/extensions/xep-0363.html)TotalSize | -1 for n/a, 0 for no limit or number in megabytes (MB)| 0 or >= 100 | >= 0
maximum[HttpFileUpload](https://xmpp.org/extensions/xep-0363.html)StorageTime | -1 for n/a, 0 for no limit or number in days | 0 or >= 7 | >= 0
maximum[MessageArchiveManagement](https://xmpp.org/extensions/xep-0313.html)StorageTime | -1 for n/a, 0 for no limit or number in days | 0 or >= 7 | >= 0
professionalHosting | true if hosted in data center with regular backups, otherwise false | true | true or false
freeOfCharge | true if unpaid service, otherwise false | true | true or false
legalNotice | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to link of legalNotice language version or {} for n/a | >= 1 | >= 0
serverLocations | list of lower-case [two-letter country codes](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) or [] for n/a | >= 1 | >= 0
groupChatSupport | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of group chat addresses or {} for n/a | >= 1 or chatSupport >= 1 or emailSupport >= 1 | >= 0
chatSupport | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of chat addresses or {} for n/a | >= 1 or groupChatSupport >= 1 or emailSupport >= 1 | >= 0
emailSupport | mappings from lower-case [two-letter (639-1) language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) to list of email addresses or {} for n/a | >= 1 or groupChatSupport >= 1 or chatSupport >= 1 | >= 0
onlineSince | [ISO YYYY-MM-DD date](https://en.wikipedia.org/wiki/ISO_8601#Dates) or "" for n/a | > 365 | >= 0 or no date

## Filter Script

The script `filter.py` can be used to create filtered lists of providers (called **provider lists**).

### Usage

You can see all options for running the script:
```
./filter.py -h
```

Provider lists for all categories are created if you run the script without arguments:
```
./filter.py
```

If you want to create a filtered list which can be used by a client, simply enter the category name.

Example for creating a list of category **A** providers:
```
./filter.py -A
```

You can create a provider list containing all providers for completely customized filtering (e.g., by an own filter script or at runtime):
```
./filter.py -C
```

A **s**imple list containing only the domains of the providers is also possible.

Example for creating a domain list of category **C** providers to use it only for autocomplete:
```
./filter.py -s -C
```

You can create files containing the **r**esults of the filtering.
Those files include the providers' properties that do not meet the criteria for being in specific categories.

Example for creating result files for all providers:
```
./filter.py -r
```

If you are interested in specific providers, you can append them to the command.

Example for creating a list of category **B** providers out of *example.org* and *example.com*:
```
./filter.py -B example.org example.com
```

The script can be run in **d**ebug mode to see why providers are not in a specific category.

Example for creating a list of category **A** providers and logging additional information:
```
./filter.py -d -A
```

Furthermore, the arguments can be combined to show which criteria specific providers do not meet for being in a specific category.

Example for creating a list of category **A** providers out of *example.org* and *example.com* and logging additional information:
```
./filter.py -d -A example.org example.com
```

## Badge Script

The script `badge.py` can be used to create badges for the specified categories.
It uses the files created by the filter script.
Thus, the filter script must be run before running the badge script.

Template files are used for generating the badges.
All badges contain the category.
Additionally, it is possible to create badges containing the count of providers in a specific category.

The badge script has two phases:
1. It generates badges for all categories.
1. It creates the directory `badges` and fills it with badges for all providers (called **provider badges**).

### Usage

You can see all options for running the script:
```
./badge.py -h
```

Badges for all categories are created if you run the script without arguments:
```
./badge.py
```

The badges for providers can be created as symbolic **l**inks instead of regular files:
```
./badge.py -l
```

If you want to create a badge which can be used by a provider, simply enter the category name.

Example for creating a badge for category **A**:
```
./badge.py -A
```

Badges containing the **c**ount of providers in specific categories are also possible.

Example for creating a badge containing the count of providers in category **C** in addition to the normal badges:
```
./badge.py -c -C
```

The script can be run in **d**ebug mode to see detailed information about the process.

Example for creating a badge for category **A** and logging additional information:
```
./badge.py -d -A
```
