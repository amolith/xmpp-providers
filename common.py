#!/usr/bin/env python3

"""
This file includes parts needed by all scripts.
"""

from argparse import ArgumentParser
from enum import Enum
import json
import logging
import os
import shutil

PROVIDERS_FILE_PATH = "providers.json"
JSON_OUTPUT_INDENTATION = "\t"

class Category(Enum):
	ALL = "a"
	AUTOMATICALLY_CHOSEN = "A"
	MANUALLY_SELECTABLE = "B"
	USABLE_FOR_AUTOCOMPLETE = "C"

def create_parent_directories(file_path):
	"""Creates all parent directories of a file.

	Parameters
	----------
	file_path : str
		path of the file
	"""

	directory_path = os.path.dirname(file_path)

	if len(directory_path) != 0:
		os.makedirs(directory_path, exist_ok=True)

def delete_parent_directory(file_path):
	"""Deletes the parent directory of a file.

	Parameters
	----------
	file_path : str
		path of the file
	"""

	directory_path = os.path.dirname(file_path)

	if len(directory_path) != 0:
		shutil.rmtree(directory_path)
